# Bandit 1

Link: [https://overthewire.org/wargames/bandit/bandit1.html](https://overthewire.org/wargames/bandit/bandit1.html)

## Challenge details

The password for the next level is stored in a file called `readme` located in the `home directory`. Use this password to log into `bandit1` using SSH. Whenever you find a password for a level, use SSH (on port 2220) to log into that level and continue the game.

## Solution

Since we are logged is as `bandit0` from the previous level, we can perform linux commands on the system.

What the challenge brief tells use that the password for `bandit1` is located in the `home directory` and in a file called `readme`.

What we can do is run the `pwd` command, which prints out onto the terminal our present working directory, or the directory that we are currently in.

When we run the commannd, we see that we're in the `/home/bandit0` directory, or the home of the `bandit0` user. Running the `ls` command, which allows to list the contents of a directory, we can see that one of the files in this folder is the `readme` file.

We can read the contents of the file using the `cat` command, which prints out the file to the command line.

Once we run the `cat` command on the `readme` file, we get the password for logging in as `bandit1`

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit1@bandit.labs.overthewire.org -p 2220
Password: boJ9jbbUNNfktd78OOpsqOltutMc3MY1
        </pre>
</details>
