# Bandit 0

Link: [https://overthewire.org/wargames/bandit/bandit0.html](https://overthewire.org/wargames/bandit/bandit0.html)

## Challenge Details

The goal of this level is for you to log into the game using SSH. The host to which you need to connect is `bandit.labs.overthewire.org`, on `port 2220`. The username is `bandit0` and the password is bandit0. Once logged in, go to the [Level 1](https://overthewire.org/wargames/bandit/bandit1.html) page to find out how to beat Level 1.

## Solution

This is a really simple challenge that just teaches you how to SSH into a remote system.

The default port for Secure Shell (SSH) is port 22 and this challenge is asking you to SSH onto port 2220, which is the non standard SSH port.

You can read the `man` pages for SSH to discover the `-p` options which will allow you to specify which port you SSH into.

In this scenario, you have to SSH onto port 2220, so the command you will run is:

`ssh bandit0@bandit.labs.overthewire.org -p 2220`

Once we run this command in our terminal, we are prompted for the password which is: `bandit0`.

Once we successfully login to the bandit systems as the user `bandit0`, we can move on to the next challenge to find out about what we need to do to log in as `bandit1`!

<details>
    <summary>Details for this level</summary>
        <pre>
SSH: ssh bandit0@bandit.labs.overthewire.org -p 2220
Password: bandit0
        </pre>
</details>
