# Bandit 5

Link: [https://overthewire.org/wargames/bandit/bandit5.html](https://overthewire.org/wargames/bandit/bandit5.html)

## Challenge Details

The password for the next level is stored in the only `human-readable` file in the `inhere` directory. Tip: if your terminal is messed up, try the “reset” command.

## Solution

So first we `cd` to the `inhere` directory by running `cd inhere`.

Then we can check out all the files in this directory by running the `ls -al` command.

We can see that there are 10 files in here and only one of them, according to the challenge details is human readable.

What we can run is the `file` command which tells us what format a file is in.

When we run the command `file ./*` we can get the file type of all the files in this directory.

From the results of this command we can see that all of the files except for `-file07` are data file, and `-file07` is in the ASCII text format.

When we run the `cat` command on `-file07` we get the password for the next level and the `bandit5` user! (Make sure to run the `cat` command on the file using `./` as the `-` character is still a special character even with text after it!)

Note: What you can also do is brute force this challenge by running `cat` on each of the files on the `inhere` directory, but using `file` is much more efficient.

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit5@bandit.labs.overthewire.org -p 2220
Password: koReBOKuIDDepwhWk7jZC0RTdopnAYKh
        </pre>
</details>
