# Bandit 9

Link: [https://overthewire.org/wargames/bandit/bandit9.html](https://overthewire.org/wargames/bandit/bandit9.html)

## Challenge Details

The password for the next level is stored in the file `data.txt` and is the only line of text that `occurs only once`

## Solution

We'll be needing to use the `sort` and `uniq` to solve this challenge.

First we need to run `data.txt` through the `sort` command as the `uniq` command is not able to detect duplicate lines unless they are adjacent.

We run the command `sort data.txt | uniq -u`, this will sort out the `data.txt` file in alphabetical order so that `uniq` with the `-u` flag can find the one unique line.

The `|` operator, also know as the pipe operator, allows you to redirect the output of one command into another command. In this case, the results of the `sort data.txt` will be piped into the `uniq -u` command.

When we run this command, we get the password for the `bandit9` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit9@bandit.labs.overthewire.org -p 2220
Password: UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR
        </pre>
</details>
