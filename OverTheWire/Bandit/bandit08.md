# Bandit 8

Link: [https://overthewire.org/wargames/bandit/bandit8.html](https://overthewire.org/wargames/bandit/bandit8.html)

## Challenge Details

The password for the next level is stored in the file `data.txt` next to the word `millionth`

## Solution

We can run the `ls -al` command to see what's files and directories that we have to deal with. We see that we have a `data.txt` file in the directory with a huge number of words and possible passwords in.

From the challenge details, we know that the password for the next user is in the `data.txt` file and is next to the word `millionth` in that file.

What we can use to find this is the `grep` command, which is a command line tool that allows us to search plain text files for matching text that we supply the command.

The command that we can run is `grep millionth data.txt`. This will return to use the line in the file `data.txt` that contains the word `millionth`.

When we run the command, we get the password to the `bandit8` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit8@bandit.labs.overthewire.org -p 2220
Password: cvX2JJa4CFALtqS87jk27qwqGhBM9plV
        </pre>
</details>
