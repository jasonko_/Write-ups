# Bandit 12

Link: [https://overthewire.org/wargames/bandit/bandit12.html](https://overthewire.org/wargames/bandit/bandit12.html)

## Challenge Details

The password for the next level is stored in the file `data.txt`, where all `lowercase (a-z) and uppercase (A-Z) letters` have been rotated by `13 positions`.

## Solution

For this challenge, the contents of the `data.txt` file have been encoded with the [ROT 13](https://en.wikipedia.org/wiki/ROT13) substitution cipher and we need to decipher this for the password to the next level.

What we need to do is to `cat` the contents of the `data.txt` file and then copy the text so that we can decipher the code using an online utility.

I recommend using the [CyberChef](https://gchq.github.io/CyberChef/#recipe=ROT13(true,true,13)) module to decipher this as it's quite simply and easy to use and it has a huge range of tools that you can use for most of your basic crytography needs.

Once the text is deciphered, we get the password for the `bandit12` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit12@bandit.labs.overthewire.org -p 2220
Password: 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu
        </pre>
</details>
