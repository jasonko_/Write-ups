# Bandit 4

Link: [https://overthewire.org/wargames/bandit/bandit4.html](https://overthewire.org/wargames/bandit/bandit4.html)

## Challenge Details

The password for the next level is stored in a `hidden file` in the `inhere directory`.

## Solution

When we run the `ls -al` command, we see that there is an new directory called `inhere`. We can go to that directory using the `cd` or change directory command.

We run the command `cd inhere` to get into the `inhere` directory then we run the `ls -al` command to get a long listing of all the files, inclusive of the hidden files inside the current directory.

Once we run `ls -al` we can see that there is a hidden file called `.hidden`. We can read the contents of this file using the `cat` command.

We get the password for the next level when we run the `cat .hidden` command and we can SSH in as `bandit4`!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit4@bandit.labs.overthewire.org -p 2220
Password: pIwrPrtPN36QITSp3EQaw936yaFoFgAB
        </pre>
</details>
