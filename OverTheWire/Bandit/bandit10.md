# Bandit 10

Link: [https://overthewire.org/wargames/bandit/bandit10.html](https://overthewire.org/wargames/bandit/bandit10.html)

## Challenge Details

The password for the next level is stored in the file `data.txt` in one of the few `human-readable strings`, beginning with `several ‘=’ characters`.

## Solution

For this challenge, we'll need to make use of the `strings` command line utility to print out the readable lines of the `data.txt` file.

We can then pipe the output of the `strings` command into the `grep` command so that we can find all the lines that have a `=` sign in them.

The command that we want to run is `strings data.txt | grep "="`.

This will return all the lines that have a `=` and we can see the password for the `bandit10` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit10@bandit.labs.overthewire.org -p 2220
Password: truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk
        </pre>
</details>
