# Bandit 6

Link: [https://overthewire.org/wargames/bandit/bandit6.html](https://overthewire.org/wargames/bandit/bandit6.html)

## Challenge Details

The password for the next level is stored in a `file` somewhere under the `inhere directory` and has all of the following properties:

* human-readable
* 1033 bytes in size
* not executable

## Solution

We can run the `ls -al` to find out what directories are files we have access to. We know that the password is stored in a file inside the `inhere` directory.

When we `cd` into the `inhere` directory, we see that there are 20 different folders with more files inside each of those folders. Brute forcing in this situation will not be feasible unless you want to script the brute force method or you have a spare 15 minutes on hand.

Instead, what we can use is the `find` command. This command with the appropriate options set will allow us to find the specific file detailed in the challenge description. We are looking for a file that is `human-readable`, `1033 bytes` and `non executable`.

What we will need to do is check out the manual page for the `find` command by typing in `man find` which will give us a list of all the options that we can use with the `find` command.

The options that we will be using are:

* `-type f`
  * This option lets us look for files of type [value], the value that we are looking for is human readable or regular file, which is what the value `f` denotes.
* `-size 1033c`
  * This option allows us to only return files that are of a certain size. The `c` in this instance means that we are looking for bytes as opposed to other data sizes.
* `! -executable`
  * The `-executable` options allows us to search for files that are executable or directories that are searchable. The reason that we put the `!` character there is that `!` is a bitwise operator which represents the `NOT` operation, which basically means opposite. When we combine the `!` operator with the `-executable` options, we're asking linux to find files that are `NOT` executable.

The command that we run while we're in the `inhere` directory is `find . -type f -size 1033c ! -executable`.

This will return the location of the file that fits the specified criteria, which is `./maybehere07/.file2`.

Now we can run `cat` on this file to get the password for the `bandit6` user!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit6@bandit.labs.overthewire.org -p 2220
Password: DXjZPULLxYr17uwoI01bNLQbtFemEgo7
        </pre>
</details>
