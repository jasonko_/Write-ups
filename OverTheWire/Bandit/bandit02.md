# Bandit 2

Link: [https://overthewire.org/wargames/bandit/bandit2.html](https://overthewire.org/wargames/bandit/bandit2.html)

## Challenge Details

The password for the next level is stored in a file called `-` located in the `home directory`.

## Solution

Now that we're logged in as `bandit1`, we can start looking for the password for the next user.

If we run the `ls -al` command, we can print a long listing of all of the contents of the current directory to the terminal, including all the hidden files and directories which are usually appended with a dot (`.`).

For example, the `.bashrc` file will not show up if you run the `ls` command, but it will shop up if you run the `ls -a` command.

Once we run the `ls -al` command, we see a file called `-`. We know that the password for the next level is stored in this file, but we also cannot read the file normally with the `cat` command by running `cat -`.

Since in the linux command line, the character `-` is used to specify additional options to run with commands, if we were to run the `cat -` command, linux would not print the contents of the `-` file as it expects us to specify options to run the `cat` command with.

What we can do instead is run the command `cat ./-`, which basically escapes the usage of the `-` character to specify command options. The `.` symbolises the current directory that we are in and the `/` character escapes the special `-` character.

When we run the command, we get the password for logging into `bandit2`!

Note: an alternative way to read this file would be using the command line text editor `vim` as a file manager by running the command `vim .` which would open up the current directory in the `vim` file manager. Navigating through the files using the `h j k l` keys as `left down up right` respectively, we can navigate through the directory and open up the `-` file to read the password for the next level!

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit2@bandit.labs.overthewire.org -p 2220
Password: CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9
        </pre>
</details>
