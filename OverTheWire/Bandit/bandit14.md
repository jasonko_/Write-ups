# Bandit 14

Link: [https://overthewire.org/wargames/bandit/bandit14.html](https://overthewire.org/wargames/bandit/bandit14.html)

## Challenge Details

The password for the next level is stored in `/etc/bandit_pass/bandit14` and can only be read by user `bandit14`. For this level, you don’t get the next password, but you get a `private SSH key` that can be used to log into the next level. Note: localhost is a hostname that refers to the machine you are working on.

## Solution

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit14@bandit.labs.overthewire.org -p 2220
Password:
        </pre>
</details>
