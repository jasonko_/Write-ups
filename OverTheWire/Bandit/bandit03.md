# Bandit 3

Link: [https://overthewire.org/wargames/bandit/bandit3.html](https://overthewire.org/wargames/bandit/bandit3.html)

## Challenge Details

The password for the next level is stored in a file called `spaces in this filename` located in the `home directory`.

## Solutions

So when we run the `ls -al` command, we can see that there is a file called, `spaces in this filename`.

The issue here is that the file name contains spaces between the multiple words, if we were to run `cat spaces in this filename`, the `cat` command would attempt to read four seperate files named, `spaces`, `in`, `this` and `filename` because `cat` would treat the spaces as seperators and attempt to read multiple files.

There are a few solutions that we can pursue to solve this challenge.

### Solution 1

Using the `cat` command and escaping the spaces in the filename with the `\` backslash character. Spaces are special characters in linux and using the `\` command allows us to _escape_ these special characters.

The command we would run would be `cat spaces\ in\ this\ filename` and this should print out the password for the next level.

### Solution 2

Using the `cat` command but encapsulating the file name with spaces inside `""` quaotation marks. This way, the `cat` command would treat the words inside the quotation marks as once work, which is the name of the file we want to read.

The command we would run would be `cat "spaces in this filename"` and this should print out the password for the next elvel.

### Solution 3

As we did previously in `bandit2` we can use the `vim` command line text editor as a file manager and select out file to read, which is `spaces in this filename`.

What we would run would be the `vim .` command to open up `vim` as a file manager in out current directory, then we would navigate to the file using the `h j k l` navigation keys for `vim` then press the `enter` key on the file to open it. This should allow us to view the contents of the file.

<details>
    <summary>Details for next level</summary>
        <pre>
SSH: ssh bandit3@bandit.labs.overthewire.org -p 2220
Password: UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK
        </pre>
</details>
